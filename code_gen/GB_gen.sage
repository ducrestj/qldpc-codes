load("bmatRW.sage")
load("code_properties.sage")

def genGB_from_poly(filename,l,a_exps,b_exps):
	P = GF(2)[x]
	R = GF(2)[x].quotient(x^l-1)
	a = P(0)
	b = P(0)
	for e in a_exps:
		a+=P(x^e)
	for e in b_exps:
		b+=P(x^e)
	print(a)
	print(b)
	k = 2*gcd(gcd(a,b),P(x^l-1)).degree()
	print("2*gcd(a,b,x^l-1) = "+str(k)+"\n")
	
	A = R(a).matrix()
	B = R(b).matrix()

	Hx,Hz = genGB(A,B)
	write_alist(filename+".HX",Hx)
	write_alist(filename+".HZ",Hz)
	
def genGB(A,B):
	if(A*B != B*A):
		print("error: the two matrices A and B must commute !")
		return
	Hx = A.augment(B)
	Hz = B.transpose().augment(A.transpose())
	print_code_properties(Hx,Hz)
	return Hx,Hz
	
