load("GB_gen.sage")

#generate codes A1 to A6 from paper BP-osd

# code A1 [254,28,13<d<21]
# l = 127	
# a(x) = 1+x^15+x^20+x^28+x^66
# b(x) = 1+x^58+x^59+x^100+x^121

a_exps=[0,15,20,28,66]
b_exps=[0,58,59,100,121]
genGB_from_poly("A1",127,a_exps,b_exps)

# code A2 [126,28,8]
# l = 63
# a(x) = 1+x+x^14+x^16+x^22
# b(x) = 1+x^3+x^13+x^20+x^42

a_exps=[0,1,14,16,22]
b_exps=[0,3,13,20,42]
genGB_from_poly("A2",63,a_exps,b_exps)

# code A3 [48,6,8]
# l = 24
# a(x) = 1+x^2+x^8+x^15
# b(x) = 1+x^2+x^12+x^17

a_exps=[0,2,8,15]
b_exps=[0,2,12,17]
genGB_from_poly("A3",24,a_exps,b_exps)

# code A4 [46,2,9]
# a(x) = 1+x^5+x^8+x^12
# b(x) = 1+x+x^5+x^7

a_exps=[0,5,8,12]
b_exps=[0,1,5,7]
genGB_from_poly("A4",23,a_exps,b_exps)

# code A5 [180,10,14<d<19]
# l = 90
# a(x) = 1+x^28+x^80+x^89
# b(x) = 1+x^2+x^21+x^25

a_exps=[0,28,80,89]
b_exps=[0,2,21,25]
genGB_from_poly("A5",90,a_exps,b_exps)

# code A6 [900,50,15]
# l = 450
# a(x) = 1+x^97+x^372+x^425
# b(x) = 1+x^50+x^265+x^390

a_exps=[0,97,372,425]
b_exps=[0,50,265,390]
genGB_from_poly("A6",450,a_exps,b_exps)


