load("GB_gen.sage")

#generate codes from threshold plot from paper BP-osd (asked to Panteleev by email)

# code T1 [126,12]
# l = 63	
# a(x) = 1 + x^43 + x^37
# b(x) = 1 + x^59 + x^31

a_exps=[0,43,37]
b_exps=[0,59,31]
genGB_from_poly("T1",63,a_exps,b_exps)

# code T2 [254,14]
# l = 127
# a(x) = 1 + x^18 +x^53
# b(x) = 1 + x^12 + x^125

a_exps=[0,18,53]
b_exps=[0,12,125]
genGB_from_poly("T2",127,a_exps,b_exps)

# code T3 [510,16]
# l = 255
# a(x) = 1 + x^250 + x^133
# b(x) = 1 + x^41 + x^157

a_exps=[0,250,133]
b_exps=[0,41,157]
genGB_from_poly("T3",255,a_exps,b_exps)

# code T4 [1022,18]
# l = 511
# a(x) = 1 + x^244 + x^157
# b(x) = 1 + x^51 + x^454

a_exps=[0,244,157]
b_exps=[0,51,454]
genGB_from_poly("T4",511,a_exps,b_exps)

# code T5 [8190,24]
# l = 4095
# a(x) = 1 + x^2083 + x^2212
# b(x) = 1 + x^1802 + x^3220 

a_exps=[0,2083,2212]
b_exps=[0,1802,3220]
genGB_from_poly("T5",4095,a_exps,b_exps)
