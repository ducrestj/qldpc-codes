load("bmatRW.sage")
load("code_properties.sage")

def genHGP(A,B):
	mA = A.nrows()
	nA = A.ncols()
	mB = B.nrows()
	nB = B.ncols()
	InA = identity_matrix(GF(2),nA)
	ImA = identity_matrix(GF(2),mA)
	InB = identity_matrix(GF(2),nB)
	ImB = identity_matrix(GF(2),mB)	
	
	Hx = A.tensor_product(ImB).augment(ImA.tensor_product(B))
	Hz = InA.tensor_product(B.transpose()).augment(A.transpose().tensor_product(InB))
	print_code_properties(Hx,Hz)
	return Hx,Hz


