###### Package for binary matrices read/write #############
# Functionalities :
# -----------------
# (for alist format)
# read_alist(filename)
# write_alist(filename,M)
# 
# (for dense matrix format)
# read_dmat(filename)
# write_dmat(filename,M)
#



# write an array of values
def write_array(f,l):
	s = ""
	for e in l:
		s+=str(e)+" "
	s+="\n"
	f.write(s)
	return

# write one column (with added zeros)
def write_col(f,M,j,max_cols):
	counter = 0
	s = ""
	m = M.nrows()
	for i in range(m):
		if M[i,j]==1:
			s+=str(i+1)+" "
			counter +=1
	for k in range(max_cols-counter):
		s+="0 "
	s+="\n"
	f.write(s)	
	return

# write one row (with added zeros)
def write_row(f,M,i,max_rows):
	counter = 0
	s = ""
	n = M.ncols()
	for j in range(n):
		if M[i,j]==1:
			s+=str(j+1)+" "
			counter +=1
	for k in range(max_rows-counter):
		s+="0 "
	s+="\n"
	f.write(s)
	return
		

# read matrices in alist format
# note that even if the weights are not all equals to max_cols (and similarly for max_rows), additional zeros are added so that each line has exactly max_cols/max_rows values. 
def read_alist(filename):
	i = 0
	file = open(filename+".alist","r")
	values = [Integer(e) for e in file.read().split()] 
	n,m,max_cols,max_rows = values[i:i+4] ; i+=4
	hw_cols = values[i:i+n] ; i+=n
	hw_rows = values[i:i+m] ; i+=m
	M = matrix(GF(2),m,n)
	for j in range(n):
		indices = values[i:i+max_cols] ; i+=max_cols
		for k in indices:
			if k!=0:
				M[k-1,j] = 1
	return M

# write matrix in alist format adding zeros so that each line has max_cols/max_rows values
def write_alist(filename,M):
	n = M.ncols()
	m = M.nrows()
	hw_cols = [0 for j in range(n)]
	hw_rows = [0 for i in range(m)]
	for i in range(m):
		for j in range(n):
			hw_cols[j] += Integer(M[i,j])
			hw_rows[i] += Integer(M[i,j])
	max_cols = max(hw_cols)
	max_rows	= max(hw_rows)
	f = open(filename+".alist","w+")
	write_array(f,[n,m])
	write_array(f,[max_cols,max_rows])
	write_array(f,hw_cols)
	write_array(f,hw_rows)
	for j in range(n):
		write_col(f,M,j,max_cols)
	for i in range(m):
		write_row(f,M,i,max_rows)
	f.write("\n")
	return

# A dense matrix is defined as :
# m n
# [m lines of n values]	
def read_dmat(filename):
	file = open(filename+".alist","r")
	values = [Integer(e) for e in file.read().split()] 
	m,n = values[0:2]
	M = matrix(GF(2),m,n,values[2:])
	return M
	
def write_dmat(filename,M):
	n = M.ncols()
	m = M.nrows()
	f = open(filename+".alist","w+")
	write_array(f,[m,n])
	for i in range(m):
		write_array(f,M[i])
	return
