# qLDPC Codes

Compilation of all the codes used for simulations.
The Hx and Hz matrices are given in the alist format (filled with additional zeros so that each line has maxRowDeg/maxColDeg entries). 
The alist format is defined there: http://www.inference.org.uk/mackay/codes/alist.html

Also provided for each code are the generator and logical operators files in a modified alist format called pcm for Parity-Check-Matrix, including informations such as girth, etc.

Finally, in each folder, a spec.txt file explains how all the matrices where generated, and additional properties (known bound on the minimum distance, etc).

## Generation of the codes
The sage code used to generate the codes is given in the folder code_gen.
Note that the generating files should be called from the code_gen directory. 
_eg: sage family\_gen/GB\_A\_codes.sage_

## Code Families

### GB Codes
Generalized bicycles codes proposed in KP13[^KP13] are constructed from two commuting matrices A and B.
They are especially easy to construct by taking circulant matrices. In that case, there is a simple formula to compute the dimension of the code ( see PK19[^PK19] for the proof of the formula).

* A codes : example codes from PK19 [^PK19]
* T family : threshold family of codes from PK19 [^PK19]

---

[^KP13]: “Quantum kronecker sum-product low-density parity-check codes with finite rate”,
A. Kovalev and L. P. Pryadko 
[^PK19]: "Degenerate Quantum LDPC Codes With Good Finite Length Performance", 
Pavel Panteleev and Gleb Kalachev
